import logging

import click
import click_rich_help
import ruamel.yaml

import syosetsu
import syosetsu.config
import syosetsu.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=syosetsu.rich.theme,
    short_help='Send error log to [primary]sentry.io[/] for testing purpose.',
)
def command() -> None:
    """
    Send error log to [primary]sentry.io[/] to test whether sentry is working
    or not.

    Please make sure that sentry is [success]enabled[/] in config.

    """
    log = logging.getLogger(__name__)
    log.info('Syosetsu->Sentry command started.')
    try:
        run()
    except Exception as exc:
        syosetsu.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('Syosetsu->Sentry command finished.')


def run() -> None:
    yaml = syosetsu.config.Path('.yml')
    if not yaml.instance.exists():
        syosetsu.rich.console.print(
            'Configuration file is missing. '
            'Please create one using [info]config[/] command.'
        )
        return

    config = ruamel.yaml.YAML(typ='safe').load(yaml.instance)

    if not config['sentry']['enabled']:
        syosetsu.rich.console.print(
            'Sentry is [danger]disabled[/] in configuration file.'
        )
        syosetsu.rich.console.print(
            f'Please [success]enable[/] sentry in [info]{yaml.instance}[/] '
            'configuration file.'
        )
        return

    logging.getLogger(__name__).error(
        'Error log example send from '
        f'{syosetsu.__name__}  Version {syosetsu.__version__}'
    )
