import functools
import importlib.resources
import pathlib


class File(object):

    def __init__(self, uri: str) -> None:
        """
        Convenient object to resource file.

        You should use relative position to the file.

        .. code-block:: python
            authors = File('authors.rst')
            yml = File('configs/.yml')

        :param uri: Relative path to the file in res folder.
        :type uri: str
        """
        self.uri: str = uri

    @functools.cached_property
    def anchor(self) -> str:
        package = 'syosetsu.res'
        index = self.uri.rfind('/')
        if index > 0:
            folder = self.uri[0:index]
            package += f'.{folder.replace("/", ".")}'
        return package

    @functools.cached_property
    def name(self) -> str:
        index = self.uri.find('/')
        if index:
            return self.uri.split('/')[-1]

        return self.uri

    @property
    def path(self) -> pathlib.Path:
        files = importlib.resources.files(self.anchor)
        with importlib.resources.as_file(files / self.name) as path:
            return path
