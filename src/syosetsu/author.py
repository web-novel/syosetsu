import logging

import click
import click_rich_help
import rich_rst

import syosetsu
import syosetsu.res
import syosetsu.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=syosetsu.rich.theme,
    short_help='See authors.',
)
def command() -> None:
    """
    See authors.

    """
    log = logging.getLogger(__name__)
    log.info('Syosetsu->Authors command started.')
    try:
        run()
    except Exception as exc:
        syosetsu.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('Syosetsu->Authors command finished.')


def run() -> None:
    syosetsu.rich.console.print(rich_rst.RestructuredText(
        syosetsu.res.File('authors.rst').path.read_text()
    ))
