import logging
import pathlib
import shutil

import click
import click_rich_help
import platformdirs

import syosetsu
import syosetsu.res
import syosetsu.rich
import syosetsu.win32.message


class Path(object):

    def __init__(self, name: str) -> None:
        """
        Convenience object to config file.

        :param name: The config file.
        :type name: str

        """
        self.name = name

    @property
    def default(self) -> pathlib.Path:
        """
        Path to default config file.

        """
        return syosetsu.res.File(f'configs/{self.name}').path

    @property
    def instance(self) -> pathlib.Path:
        """
        Path to config file in pyInstaller executable if the application is
        frozen or user config directory in any other case.

        """
        return (
            syosetsu.PATH
            if syosetsu.FROZEN
            else platformdirs.user_config_path(
                syosetsu.__title__,
                syosetsu.__author__,
            )
        ) / self.name


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=syosetsu.rich.theme,
    short_help='Initialize Syosetsu.',
)
@click.confirmation_option(
    prompt='Overwrite existing configuration file if exists?',
)
def command() -> None:
    """
    Create configuration file [primary].yml[/] and [primary].env[/] with
    default value in current directory.

    """
    log = logging.getLogger(__name__)
    log.info('Syosetsu->Init command started.')
    try:
        run()
    except Exception as exc:
        syosetsu.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('Syosetsu->Init command finished.')


def run() -> None:
    yml = Path('.yml')
    shutil.copy(yml.default, yml.instance)
    syosetsu.rich.console.print(
        f'[info].yml[/] file created at [info]{yml.instance}[/]'
    )

    env = Path('.env')
    shutil.copy(env.default, env.instance)
    syosetsu.rich.console.print(
        f'[info].env[/] file created at [info]{env.instance}[/]'
    )
