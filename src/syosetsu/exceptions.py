class ChapterMissingError(Exception):
    pass


class RepoError(Exception):
    pass


class SessionAuthError(Exception):
    pass


class SessionGetError(Exception):
    pass
