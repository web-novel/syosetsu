import logging
import os
import pathlib

import click
import click_rich_help

import syosetsu.exceptions
import syosetsu.folder
import syosetsu.http
import syosetsu.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=syosetsu.rich.theme,
    short_help='Download new chapter(s) from Syosetsu.',
)
def command() -> None:
    """
    This command will automatically continue download from the last chapter.

    """
    log = logging.getLogger(__name__)
    log.info('Syosetsu->Download command started.')
    try:
        run()
    except Exception as exc:
        syosetsu.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('Syosetsu->Download command finished.')


def run() -> None:
    repo = syosetsu.folder.Repository(pathlib.Path(os.getcwd()))
    if not repo.validate():
        syosetsu.rich.console.print(
            'This directory is not a valid Syosetsu repository.'
        )
        return

    session = syosetsu.http.Session(repo.id)
    try:
        session.login(
            str(repo.config['username']),
            str(repo.config['password']),
        )
    except syosetsu.exceptions.SessionAuthError as exc:
        syosetsu.rich.console.print(exc)
        return

    index = repo.last

    while True:
        index += 1
        syosetsu.rich.console.print(f'Downloading chapter {index}', end='... ')
        try:
            response = session.get(index)
        except syosetsu.exceptions.ChapterMissingError:
            syosetsu.rich.console.print(
                '[danger]Missing![/] '
                f'It seems chapter {index - 1} is the latest one.'
            )
            break

        except syosetsu.exceptions.SessionGetError as exc:
            syosetsu.rich.console.print('[danger]Error![/]')
            syosetsu.rich.console.print(exc)
            logging.getLogger(__name__).error(
                exc,
                exc_info=exc,
                stack_info=True,
            )
            return

        repo.add(index, response.content)
        session.sleep()
