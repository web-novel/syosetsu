import builtins
import logging
import logging.config
import pathlib

import click
import click_rich_help
import platformdirs
import ruamel.yaml

import syosetsu
import syosetsu.author
import syosetsu.config
import syosetsu.download
import syosetsu.history
import syosetsu.init
import syosetsu.rich
import syosetsu.sentry
import syosetsu.win32.message


@click.group(
    cls=click_rich_help.StyledGroup,
    theme=syosetsu.rich.theme,
)
@click.version_option(version=syosetsu.__version__, message='%(version)s')
@click.pass_context
def group(ctx) -> None:
    """
    \b
    [option]Syosetsu Downloader[/]
    [primary]Version 3.0.0[/]

    Syosetu text format downloader.

    [success]© 2019-2024 Andy Yulius[/]

    """
    pass


group.add_command(syosetsu.author.command, name='author')
group.add_command(syosetsu.download.command, name='download')
group.add_command(syosetsu.config.command, name='config')
group.add_command(syosetsu.history.command, name='history')
group.add_command(syosetsu.init.command, name='init')
group.add_command(syosetsu.sentry.command, name='sentry')


def run():
    yaml = syosetsu.config.Path('.yml')
    config = ruamel.yaml.YAML(typ='safe').load(
        yaml.instance
        if yaml.instance.exists()
        else yaml.default
    )

    original_log_path = pathlib.Path(
        config['logging']['handlers']['file']['filename']
    )
    if not original_log_path.is_absolute():
        log_path = (
            syosetsu.PATH
            if syosetsu.FROZEN
            else platformdirs.user_log_path(
                syosetsu.__title__,
                syosetsu.__author__,
            )
        ) / original_log_path.name
        config['logging']['handlers']['file']['filename'] = str(log_path)
        log_path.parent.mkdir(parents=True, exist_ok=True)
    logging.config.dictConfig(config['logging'])

    log = logging.getLogger(__name__)

    if not config['sentry']['enabled']:
        log.warning('Sentry is disabled.')
    else:
        log.info('Sentry is enabled.')

        import sentry_sdk
        import sentry_sdk.integrations.logging

        sentry_logging = sentry_sdk.integrations.logging.LoggingIntegration(
            level=logging.ERROR,  # Capture info and above as breadcrumbs
            event_level=logging.ERROR,  # Send errors as events
        )
        sentry_sdk.init(
            dsn=config['sentry']['dsn'],
            integrations=[
                sentry_logging,
            ],
            traces_sample_rate=config['sentry']['traces_sample_rate'],
        )

    try:
        import icecream

        icecream.install()
        log.debug(
            'IceCream installed to built-ins successfully. '
            'ic command is now available globally.'
        )

    except ImportError:  # Graceful fallback if IceCream isn't installed.
        builtins.ic = (
            lambda *a: None
            if not a
            else (a[0] if len(a) == 1 else a)
        )
        log.info(
            'IceCream module is not exists. '
            'ic command will fallback to produce nothing.'
        )

    del config

    group()


if __name__ == "__main__":
    run()
