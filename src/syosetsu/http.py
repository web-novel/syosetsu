import logging
import random
import time
import typing

import requests

import syosetsu.exceptions


class Session(object):

    def __init__(self, id: int) -> None:
        self.id = id

    def get(self, chapter: int) -> requests.Response:
        log = logging.getLogger(__name__)
        log.debug(f'Downloading chapter {chapter}.')

        url = 'https://ncode.syosetu.com/txtdownload/dlstart/ncode/{}/'
        params: typing.Dict[str, typing.Union[str, int]] = {
            'no': chapter,
            'code': 'utf-8',
            'kaigyo': 'lf',
            'hankaku': 0,
        }
        response = self.session.get(
            url.format(self.id),
            params=params,
        )
        if response.status_code == 200:
            log.debug(f'Chapter {chapter} downloaded.')
            return response

        elif response.status_code == 404:
            msg = (
                f'Chapter {chapter} is not exists. '
                f'Server returns {response.status_code}.'
            )
            log.debug(msg)
            raise syosetsu.exceptions.ChapterMissingError(msg)
        else:
            msg = f'Download error. {response.status_code}: {response.text}'
            log.debug(msg)
            raise syosetsu.exceptions.SessionGetError(msg)

    def login(self, username: str, password: str) -> None:
        agent = (
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
            'AppleWebKit/537.36 (KHTML, like Gecko) '
            'Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'
        )
        url = 'https://syosetu.com/login/login/'

        self.session = requests.Session()
        self.session.headers.update({'User-Agent': agent})

        response = self.session.post(
            url,
            data={
                'narouid': username,
                'pass': password,
                'skip': 1,
            },
        )
        if 'ログインに失敗しまし' in response.text:
            raise syosetsu.exceptions.SessionAuthError(
                'Login failed. Please check your username and password.'
            )

    @staticmethod
    def sleep() -> None:
        log = logging.getLogger(__name__)
        duration = random.randint(2, 5)
        log.debug(f'Sleep for {duration} seconds to avoid DOS like attack.')
        time.sleep(duration)
