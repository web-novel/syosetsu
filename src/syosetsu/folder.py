import functools
import logging
import pathlib
import re

import tomlkit
import tomlkit.toml_document

import syosetsu.exceptions


class Repository(object):

    def __init__(self, path: pathlib.Path) -> None:
        self.path = path
        """Path to repository directory."""

        self.toml = self.path / '.syosetsu' / '.toml'
        """Path to TOML file."""

    def add(self, index: int, data: bytes):
        """
        Add chapter <INDEX> to repository.

        """
        with (pathlib.Path(self.path) / f'{index:03d}.txt').open('wb') as fp:
            fp.write(data)

    @functools.cached_property
    def config(self) -> tomlkit.toml_document.TOMLDocument:
        if not self.validate():
            raise syosetsu.exceptions.RepoError(
                'Not a valid Syosetsu repository.'
            )
        with self.toml.open() as fp:
            return tomlkit.load(fp)

    @functools.cached_property
    def id(self) -> int:
        """
        Syosetsu novel ID.

        """
        url = str(self.config['url'])
        r = re.findall(pattern=r'/(\d+)/', string=url)

        try:
            return int(r.pop())

        except IndexError:
            raise ValueError(f'ID is missing from this URL: {url}')

    def init(self, username: str, password: str, url: str):
        log = logging.getLogger(__name__)

        folder = self.path / '.syosetsu'
        log.debug(f'Creating folder "{folder}".')
        folder.mkdir(exist_ok=True)
        log.debug(f'Folder "{folder}" created successfully.')

        doc = tomlkit.document()
        doc.add(tomlkit.comment('Syosetsu Novel Configuration'))
        doc.add(tomlkit.nl())
        doc.add('username', tomlkit.item(username))
        doc.add('password', tomlkit.item(password))
        doc.add('url', tomlkit.item(url))

        log.debug(f'Creating file "{self.toml}".')
        with self.toml.open(mode='w', encoding='utf-8') as fp:
            tomlkit.dump(doc, fp)
        log.debug(f'File "{self.toml}" created successfully.')

    @property
    def last(self) -> int:
        """
        Returns last downloaded chapter, 0 if no chapter.

        """
        last = 0
        for f in self.path.iterdir():
            if f.is_dir():
                continue
            try:
                chapter = int(f.stem)
            except ValueError:
                chapter = 0

            last = chapter if chapter > last else last

        return last

    def validate(self) -> bool:
        """
        Check whether the path is a valid repository.

        """
        return self.toml.exists()
