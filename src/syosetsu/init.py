import logging
import os
import pathlib

import click
import click_rich_help

import syosetsu.folder
import syosetsu.rich


@click.command(
    cls=click_rich_help.StyledCommand,
    theme=syosetsu.rich.theme,
    short_help=(
        'Create an empty Syosetsu novel repository or reinitialize an '
        'existing one.'
    ),
)
@click.option('--username', prompt=True)
@click.option('--password', prompt=True)
@click.option('--url', prompt='TXT download URL')
def command(username: str, password: str, url: str) -> None:
    """
    This command create an empty Syosetsu novel repository-basically a
    .syosetsu directory in the current working directory.

    You must supply your Syosetsu login credential and "TXT Download" link.

    Please note that "TXT Download" link is not link to the novel index.
    You can find the link at the bottom of the novel index page.
    It says "TXTダウンロード".

    \b
    [danger]Example:[/]
    Novel index page (real url you can open in browser):
    https://ncode.syosetu.com/n1745ct/
    TXT Download page for the novel above:
    https://ncode.syosetu.com/txtdownload/top/ncode/711674/

    """
    log = logging.getLogger(__name__)
    log.info('Syosetsu->Init command started.')
    try:
        run(username, password, url)
    except Exception as exc:
        syosetsu.rich.console.print(exc)
        log.error(exc, exc_info=exc, stack_info=True)
    log.info('Syosetsu->Init command finished.')


def run(username: str, password: str, url: str) -> None:
    repo = syosetsu.folder.Repository(pathlib.Path(os.getcwd()))
    syosetsu.rich.console.print('Initializing folder', end='... ')
    try:
        repo.init(username, password, url)
    except Exception as exc:
        syosetsu.rich.console.print('[danger]Error![/]')
        syosetsu.rich.console.print(exc)
        logging.getLogger(__name__).error(exc, exc_info=exc, stack_info=True)
    else:
        syosetsu.rich.console.print('[success]Done.[/]')
