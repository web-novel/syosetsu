===================
Syosetsu Downloader
===================

   **WARNING!**

   This program is deprecated and will be abandoned.

   Please use `Yuki <https://gitlab.com/suzuharu/yuki>`_:
   The Extensible Novel Downloader.

   Aside from Syosetsuka ni narou, Yuki also support Alphapolis.
   The chapter data is the same, so you don't have to redownload.

This program will download the web novel text version from
`Syosetsuka ni narou <http://syosetu.com>`__ or syosetu as it spell in the url.

It's simply a convenience batch interface of the official TXT Download.
This program is intended to be used in conjunction with Task Scheduler or Cron.
I don't recommend you to run this program manually,
let Task Scheduler or Cron take care to run this program periodically.
The program will continue to download the next chapter calculated from the last
chapter previously downloaded.

Because syosetu now hide the TXT Download behind user login,
I decide to overhaul the whole system.

This is version 3, it's completely different from previous version,
but the text file created is the same.
For this version I decide to try `git <https://git-scm.com>`_ pattern and store
the config in the ``.syosetsu``` folder.

* Free software: MIT license



Installation
============

To install Syosetsu Downloader, run this command in your terminal:

.. code-block:: bat

  (.venv) > python -m pip install https://gitlab.com/web-novel/syosetsu/-/archive/master/syosetsu-master.tar.gz


Usage
=====

First open the novel index page in `Syosetsu`_ website and copy the
"TXT Download" (TXTダウンロード) link from the bottom of the page.

Create a new folder where you want to create the novel repository then run
init.

.. code-block:: bat

   (.venv) > syosetsu init --username=<USERNAME> --password=<PASSWORD> --url=<URL>

The command will create ``.syosetsu`` folder and ``.toml`` file in it.

To start downloading the chapters, use ``download``` command.

.. _Syosetsu: https://syosetu.com

.. code-block:: bat

   (.venv) > syosetsu download
