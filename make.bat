@ECHO OFF
setlocal enabledelayedexpansion

pushd %~dp0

REM Command file for Syosetsu Downloader

IF EXIST build/ rm -rf build/
rm -f *.spec

IF "%1" == "" GOTO :help
IF "%1" == "exe" GOTO :warning
IF "%1" == "cli" GOTO :warning
IF "%1" == "gui" GOTO :warning
IF "%1" == "wheel" GOTO :wheel
IF "%1" == "l10n" GOTO :l10n


:help
ECHO.[96mUsage[0m: [1mmake [COMMAND] [OPTION][0m
ECHO.
ECHO.  [93mSyosetsu Downloader[0m
ECHO.  [94mVersion 3.0.0[0m
ECHO.
ECHO.[96mOptions[0m:
ECHO.  [93m--yes  [0mBypass confirmation.
ECHO.
ECHO.[96mCommands[0m:
ECHO.  [93mwheel  [0mBuild wheel distribution.
ECHO.  [93mcli    [0mBuild windows executable with console.
ECHO.  [93mgui    [0mBuild windows executable without console.
ECHO.  [93mexe    [0mBuild both windows executable.
ECHO.  [93ml10n   [0mBuild localization language.
GOTO :cleanup


:warning
IF NOT "%2" == "--yes" (
	ECHO.
	ECHO. [101;93m                                                                              [0m
	ECHO. [101;93m                         * * *   W A R N I N G   * * *                        [0m
	ECHO. [101;93m                                                                              [0m
	ECHO.
	ECHO.Due to limitation in bumpversion, you must change version in ".ffi"
	ECHO.file manually.
	ECHO.
	SET /p continue="[93mDo you want to continue?[0m [y/N]: "
	IF /i "!continue!" neq "Y" GOTO :EOF
)

CALL :l10n

IF "%1" == "exe" (
	CALL :compile console Syosetsu
	CALL :compile windowed Syosetsu-W
)
IF "%1" == "cli" CALL :compile console Syosetsu
IF "%1" == "gui" CALL :compile windowed Syosetsu-W
GOTO :cleanup


:compile
pyinstaller ^
	--onefile ^
	--%1 ^
	--name=%2 ^
	--icon=src/syosetsu/res/icons/syosetsu.ico ^
	--hidden-import=colorlog ^
	--hidden-import=pkg_resources.py2_warn ^
	--version-file=.ffi ^
	--add-data=src\syosetsu\res;.\syosetsu\res ^
	--add-data=src\syosetsu\locale\id\LC_MESSAGES\syosetsu.mo;.\locale\id\LC_MESSAGES\ ^
	src/syosetsu/main.py
GOTO :EOF

:wheel
python setup.py bdist_wheel
GOTO :cleanup


:l10n
IF NOT EXIST src\syosetsu\locale mkdir src\syosetsu\locale
pybabel ^
    extract ^
    --keywords=t ^
    --project="Syosetsu Downloader" ^
    --version="3.0.0" ^
    --copyright-holder="Andy Yulius" ^
    --msgid-bugs-address="andy.julot@gmail.com" ^
    --output-file=src\syosetsu\locale\.pot ^
    src\syosetsu
IF EXIST src\syosetsu\locale\id\LC_MESSAGES\syosetsu.po (
    pybabel update -i src\syosetsu\locale\.pot -D syosetsu -d src\syosetsu\locale
) else (
    pybabel init -i src\syosetsu\locale\.pot -D syosetsu -d src\syosetsu\locale -l id
)
pybabel compile -D syosetsu -d src\syosetsu\locale
GOTO :cleanup


:cleanup
IF EXIST build/ rm -rf build/
rm -f *.spec
popd
