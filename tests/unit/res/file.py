import syosetsu.res


def test():
    env = syosetsu.res.File('configs/.env')
    assert env.anchor == 'syosetsu.res.configs'
    assert env.name == '.env'

    authors = syosetsu.res.File('authors.rst')
    assert authors.anchor == 'syosetsu.res'
    assert authors.name == 'authors.rst'

    rst = syosetsu.res.File('docs/misc/test.rst')
    assert rst.anchor == 'syosetsu.res.docs.misc'
    assert rst.name == 'test.rst'
