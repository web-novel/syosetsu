import click.testing

import syosetsu.commands


def test():
    runner = click.testing.CliRunner()
    help_result = runner.invoke(syosetsu.commands.group, ['--help'])
    assert help_result.exit_code == 0
    assert 'Show this message and exit.' in help_result.output
