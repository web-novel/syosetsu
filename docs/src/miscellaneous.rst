.. highlight:: console

=============
Miscellaneous
=============


Development
-----------

The sources for Syosetsu Downloader can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

*  SSH
   .. code-block::

      > git clone git@gitlab.com:julot/syosetsu.git

*  HTTPS

   .. code-block::

      > git clone https://gitlab.com/julot/syosetsu.git

Once you have a copy of the source, you can install it with:

.. code-block::

   > python setup.py install

After clone or download the repo as mention above,
enter the project directory and type:

.. code-block::

   > pip install -e .[dev]

.. _GitLab repo: https://gitlab.com/julot/syosetsu


Manage TODO
^^^^^^^^^^^

`TODO.txt Homepage <https://github.com/todotxt/todo.txt>`__.

`Topydo Homepage <https://github.com/bram85/topydo>`__.

.. code-block::

   > topydo ls


Update Version
^^^^^^^^^^^^^^

`BumpVersion Homepage <https://github.com/peritus/bumpversion>`__.

.. code-block::

   > bumpversion [major | minor | patch]


PyPI Distribution
^^^^^^^^^^^^^^^^^

`Packaging Python Projects
<https://packaging.python.org/tutorials/packaging-projects>`__.

.. code-block::

   > make wheel
   > REM Test your package first
   > twine upload --repository-url https://test.pypi.org/legacy/ dist/*
   > REM Upload your package for real this time if test passed
   > twine upload dist/*


Make Executable
^^^^^^^^^^^^^^^

`PyInstaller Homepage <https://www.pyinstaller.org>`__.

*  Command Line Interface

   .. code-block::

      > make cli


*  Graphical User Interface

   .. code-block::

      > make gui

*  Both CLI and GUI at the same time

   .. code-block::

      > make exe


Internationalization (I18n) and Localization (L10n)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indonesian localization is provided in ``make`` command by default.
If you want other language, please adjust the ``make.bat`` file accordingly.

*  To compile translation file

   .. code-block::

      > make l18n

If you add or change the language,
don't forget to adjust the ``make cli`` and ``make gui`` command.

To change the language, set environment variable ``LANGUAGE``.
