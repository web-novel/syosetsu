Welcome to Syosetsu Downloader's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   miscellaneous
   contributing
   authors
   history


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
